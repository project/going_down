Going Down

A module that warns site visitors before the web site will be going down for
maintenance mode at a specified time. The default configuration displays a fixed
position red overlay message at the bottom of the browser window with the text
'<site name> is going offline for maintenance: <offline time>'.

Module configuration is added to the Configuration -> Development -> Maintenance
mode administration page (admin/config/development/maintenance). Settings allow
for the modification of: Show warning, off-line action, off-line date and time,
time format, and warning message. Off-line action includes a default option to
set the site offline automatically. There is also an advanced settings to
control the CSS display of the warning message container.
